package com.biom4st3r.moenchantments.enchantments;

import java.util.function.Function;

import com.biom4st3r.moenchantments.enchantments.MoEnchantment.LevelProvider;

import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.Enchantment.Weight;
import net.minecraft.enchantment.EnchantmentTarget;
import net.minecraft.entity.EquipmentSlot;
import net.minecraft.item.ItemStack;

/**
 * MoEnchantBuilder
 */
public class MoEnchantBuilder {
    MoEnchantment moenchant;
    //EquipmentSlot
    public static final EquipmentSlot[] 
        ARMOR_SLOTS = new EquipmentSlot[] {EquipmentSlot.CHEST,EquipmentSlot.FEET,EquipmentSlot.HEAD,EquipmentSlot.LEGS},
        ALL_SLOTS = new EquipmentSlot[] {EquipmentSlot.CHEST,EquipmentSlot.FEET,EquipmentSlot.HEAD,EquipmentSlot.LEGS,EquipmentSlot.MAINHAND,EquipmentSlot.OFFHAND},
        HAND_SLOTS = new EquipmentSlot[] {EquipmentSlot.MAINHAND,EquipmentSlot.OFFHAND};
    
    


    public MoEnchantBuilder(Weight weight,EnchantmentTarget et, EquipmentSlot... slots)
    {
        this.moenchant = new MoEnchantment(weight, et, slots);
    }


    public MoEnchantBuilder enabled(boolean bool)
    {
        this.moenchant.enabled = bool;
        return this;
    }

    public MoEnchantBuilder treasure(boolean bool)
    {
        this.moenchant.isTreasure = bool;
        return this;
    }

    public MoEnchantBuilder curse(boolean bool)
    {
        this.moenchant.isCurse = bool;
        return this;
    }

    public MoEnchantBuilder minlevel(int i)
    {
        this.moenchant.minlevel = i;
        return this;
    }

    public MoEnchantBuilder maxlevel(int i)
    {
        this.moenchant.maxlevel = i;
        return this;
    }

    public MoEnchantBuilder minpower(LevelProvider provider)
    {
        this.moenchant.minpower = provider;
        return this;
    }

    public MoEnchantBuilder maxpower(LevelProvider provider)
    {
        this.moenchant.maxpower = provider;
        return this;
    }

    public MoEnchantBuilder addExclusive(Enchantment e)
    {
        this.moenchant.exclusiveEnchantments.add(e);
        return this;
    }

    public MoEnchantBuilder isAcceptible(Function<ItemStack,Boolean> isAcceptible)
    {
        this.moenchant.isAcceptible = isAcceptible;
        return this;
    }

    public MoEnchantment build(String regname)
    {
        this.moenchant.regname = regname;
        return this.moenchant;
    }
}