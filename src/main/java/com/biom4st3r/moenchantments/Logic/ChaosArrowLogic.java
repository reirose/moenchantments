package com.biom4st3r.moenchantments.logic;

import java.util.Random;

import com.biom4st3r.moenchantments.EnchantmentRegistry;
import com.biom4st3r.moenchantments.enchantments.MoEnchantment;

import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.effect.StatusEffectInstance;
import net.minecraft.entity.projectile.ArrowEntity;
import net.minecraft.entity.projectile.ProjectileEntity;
import net.minecraft.entity.projectile.SpectralArrowEntity;
import net.minecraft.potion.Potion;
import net.minecraft.util.registry.Registry;

/**
 * ChaosArrowLogic
 */
public class ChaosArrowLogic {

    public static boolean makePotionArrow(LivingEntity attacker,ProjectileEntity projectileEntity, Random random) 
    {
        if(projectileEntity instanceof SpectralArrowEntity) return false;
        if(MoEnchantment.hasEnchant(EnchantmentRegistry.ARROW_CHAOS, attacker.getActiveItem()) && random.nextInt(3) == 0)
        {
            Potion potion = Registry.POTION.getRandom(random);
            ArrowEntity arrow = (ArrowEntity) projectileEntity;
            for(StatusEffectInstance sei : potion.getEffects())
                arrow.addEffect(new StatusEffectInstance(sei.getEffectType(),(int)Math.ceil(sei.getDuration()*0.20),sei.getAmplifier()));//Standard is .24
            return true;
        }
        return false;
	}
}