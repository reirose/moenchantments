package com.biom4st3r.moenchantments.logic;

import java.util.Random;

import com.biom4st3r.moenchantments.EnchantmentRegistry;
import com.biom4st3r.moenchantments.events.LivingEntityDamageCallback;
import com.google.common.base.Predicate;

import net.minecraft.block.BlockState;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.mob.Monster;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Box;
import net.minecraft.util.math.MathHelper;
import net.minecraft.world.World;

/**
 * Hoardering
 */
public class HoardingLogic {

    public static boolean checkBox(World world, Box box, Predicate<BlockState> condition)
    {
        boolean result = true;
        for(int x = (int) box.x1; x < box.x2; x++)
        {
            for(int y = (int) box.y1; y < box.y2; y++)
            {
                for(int z = (int) box.z1; z < box.z2; z++)
                {
                    result &= condition.apply(world.getBlockState(new BlockPos(x, y, z)));
                }
            }
        }
        return result;
    }

    public static void init()
    {
        LivingEntityDamageCallback.EVENT.register((damageSource,damage,entity,ci)->
        {
            if(damageSource.getAttacker() != null)
            {
                LivingEntity attacker = (LivingEntity) damageSource.getAttacker();
                int hoardingLvl = EnchantmentHelper.getEquipmentLevel(EnchantmentRegistry.HOARDING, attacker);
                if(hoardingLvl > 0 && !(entity instanceof PlayerEntity) && entity instanceof Monster)
                {
                    Random random = entity.getEntityWorld().getRandom();
                    World world = entity.getEntityWorld();
                    for(int i = 0; i < 10; i++);
                    {
                        int x = entity.getBlockPos().getX() + (random.nextBoolean() ? 1 : -1) * MathHelper.nextInt(random, 5, 30);
                        int y = entity.getBlockPos().getY() + (random.nextBoolean() ? 1 : -1) * MathHelper.nextInt(random, 5, 30);
                        int z = entity.getBlockPos().getZ() + (random.nextBoolean() ? 1 : -1) * MathHelper.nextInt(random, 5, 30);
                        BlockPos spawnPos = new BlockPos(x, y, z);
                        if(checkBox(world, new Box(spawnPos,spawnPos.up(3)), (blockstate)-> blockstate.isAir()))
                        {
                            world.spawnEntity(entity.getType().create(world));
                        }
                    }
                    
                }
            }
        });
    }
}