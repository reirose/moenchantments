package com.biom4st3r.moenchantments.logic;

import java.util.ArrayList;
import java.util.Queue;

import com.biom4st3r.biow0rks.Conditional;
import com.biom4st3r.moenchantments.EnchantmentRegistry;
import com.biom4st3r.moenchantments.ModInit;
import com.biom4st3r.moenchantments.interfaces.PlayerExperienceStore;
import com.biom4st3r.moenchantments.util.UniqueQueue;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.block.CropBlock;
import net.minecraft.block.LogBlock;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.entity.ExperienceOrbEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.sound.SoundCategory;
import net.minecraft.sound.SoundEvents;
import net.minecraft.stat.Stats;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.Formatting;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.registry.Registry;
import net.minecraft.world.World;

public final class VeinMinerLogic
{

	public static void tryVeinMining(World world,BlockPos blockPos,BlockState blockState,PlayerEntity pe)
    {
		if(world.isClient) return;
		if(pe==null) return ; // Patch for Adorn CarpetedBlock.
		
		ItemStack tool = pe.getMainHandStack();
		if(tool == ItemStack.EMPTY || !tool.hasEnchantments())
		{
			return;
		}
		int VeinMinerLvl = EnchantmentHelper.getLevel(EnchantmentRegistry.VEINMINER, tool);
		int TreeFellerLvl = EnchantmentHelper.getLevel(EnchantmentRegistry.TREEFELLER,tool);
		boolean hasAutoSmelt = EnchantmentHelper.getLevel(EnchantmentRegistry.AUTOSMELT, tool) > 0;

		Block currentType = blockState.getBlock();
		int brokenBlocks = 0;

		if(TreeFellerLvl > 0)
        {
            if(isValidBlockForAxe( currentType))
            {
				brokenBlocks = doVeinMiner(blockState,world,blockPos,ModInit.config.TreeFellerMaxBreakByLvl[TreeFellerLvl-1],pe,tool);
				ModInit.logger.debug("%s block broken",brokenBlocks);
			}
		}
		else if(VeinMinerLvl > 0)
		{
			if(isValidBlockForPick(currentType))
			{
				brokenBlocks = doVeinMiner(blockState,world,blockPos,ModInit.config.VeinMinerMaxBreakByLvl[VeinMinerLvl-1],pe,tool);
				ModInit.logger.debug("%s block broken",brokenBlocks);
			}
		}
		pe.increaseStat(Stats.MINED.getOrCreateStat(currentType), brokenBlocks);
		if(hasAutoSmelt)
		{
			int retrievedXp = ((PlayerExperienceStore)(Object)pe).getAndRemoveStoredExp();
			ModInit.logger.debug("retrieved XP %s", retrievedXp);
			for(; retrievedXp > 0; retrievedXp--)
			{
				dropExperience(1, blockPos, world);
			}
		}
	}

	public static ArrayList<CropBlock> getCropBlocks(World w, BlockPos source, Block type)
	{
		return null;
	}
	


	private static boolean isValidBlockForPick(Block block)
	{
		if(block == Blocks.STONE || block == Blocks.ANDESITE || block ==Blocks.GRANITE || block == Blocks.DIORITE)
		{
			return false;
		}
		else if(ModInit.int_block_whitelist.contains(Registry.BLOCK.getRawId(block)))
		{
			return true;
		}
		ModInit.logger.debug("%s global false",block.toString());
		return false;
		
	}

	private static boolean isValidBlockForAxe(Block block)
	{
		String path = Registry.BLOCK.getId(block).getPath();
		if(block instanceof LogBlock)
		{
			return true;
		}
		else if(path.endsWith("_log"))// yes this is bad
		{
			return true;
		}
		else if(path.endsWith("_wood"))// no I don't care...yet
		{
			return true;
		}
		return false;
	}
	
	private static void dropExperience(float experienceValue ,BlockPos blockPos,World world)
	{
		world.spawnEntity(new ExperienceOrbEntity(world, blockPos.getX(), blockPos.getY(), blockPos.getZ(), (int)experienceValue));
	}

	public static ArrayList<BlockPos> getSameBlocks(World w,BlockPos source, Block type)
    {
        ArrayList<BlockPos> t = new ArrayList<BlockPos>();
		for(BlockPos pos : new BlockPos[] {source.up().south(),source.up().east(),source.up().west(),source.up().north(),
											source.up(),source.down(),source.north(),source.east(),source.south(),source.west(),
											source.down().north(),source.down().east(),source.down().west(),source.down().south()})
        {
            if(w.getBlockState(pos).getBlock() == type)
            {
                t.add(pos);
            }
		}
		
        return t;
	}

	private static int doVeinMiner(BlockState blockState,World world,BlockPos blockPos,int maxBlocks,PlayerEntity pe,ItemStack tool)
	{
		ModInit.logger.debug("BlockType %s", blockState.getBlock());
		int blocksBroken = 0;
		Queue<BlockPos> toBreak = new UniqueQueue<>();//new LinkedList<BlockPos>();
		toBreak.addAll(getSameBlocks(world,blockPos,blockState.getBlock()));
		while(!toBreak.isEmpty() && blocksBroken <= (maxBlocks))
		{
			BlockPos currPos = toBreak.remove();
			if(toBreak.size() < 50)
				toBreak.addAll(getSameBlocks(world, currPos, blockState.getBlock()));

			// int maxDistance = ModInit.config.MaxDistanceFromPlayer;
			// boolean tofar = Math.abs(currPos.getX() - pe.getX()) < maxDistance && Math.abs(currPos.getZ() - pe.getZ()) < maxDistance;
			if(world.getBlockState(currPos).getBlock() == blockState.getBlock())
			{
				Block.dropStacks(world.getBlockState(currPos), world, pe.getBlockPos(), null, pe, tool);
				world.breakBlock(currPos, false);
				
				tool.damage(1, pe,(playerEntity_1) -> {
					playerEntity_1.sendToolBreakStatus(pe.getActiveHand());
				});
				if((ModInit.config.ProtectItemFromBreaking ? tool.getMaxDamage()-tool.getDamage() < 10 : true))
				{
					pe.playSound(SoundEvents.ENTITY_ITEM_BREAK, SoundCategory.PLAYERS, 1f, 1f);
					pe.sendMessage(new TranslatableText("My tool is badly damaged").formatted(Formatting.GREEN));
					break;
				}
				blocksBroken++;
			}
			world.setBlockState(currPos, Blocks.AIR.getDefaultState());
		}
		ModInit.lockAutoSmeltSound = false;
		pe.addExhaustion(0.005F * blocksBroken);
		return blocksBroken;
	}
}