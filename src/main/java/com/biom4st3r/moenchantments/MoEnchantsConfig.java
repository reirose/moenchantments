package com.biom4st3r.moenchantments;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import net.fabricmc.loader.api.FabricLoader;

public class MoEnchantsConfig
{
    public boolean EnableAutoSmelt;
    public boolean EnableEnderProtection;
    public boolean EnablePotionRetention;
    public boolean EnableTamedProtection;
    public boolean EnableTreeFeller;
    public boolean EnableVeinMiner;
    public boolean EnableSoulBound;
    public boolean EnableAccuracy;
    public boolean EnableInAccuracy;
    // public boolean EnableTrainingWeapon;
    // public boolean EnableVampirism;
    // public boolean EnableHording;
    public boolean EnableArrowChaos;


    public boolean UseStandardSoulboundMechanics;

    public int[] VeinMinerMaxBreakByLvl;

    public int[] TreeFellerMaxBreakByLvl;
    
    public int MaxDistanceFromPlayer;

    public boolean ProtectItemFromBreaking;

    public float AutoSmeltWoodModifier;

    public boolean TameProtectsOnlyYourAnimals;

    public int MaxSoulBoundLevel;

    //public boolean vanillaToolTips = false;

    public int chanceForEnderCurseToTeleport;
    public int chanceForEnderCurseToPreventDamage;

    public int perLevelChargeMultiplierForPotionRetention;
    public int PotionRetentionMaxLevel;

    public String[] veinMinerBlockWhiteList;
    //public String[] veinMinerClassWhiteList;

    public String[] AutoSmeltBlackList;

    public MoEnchantsConfig()
    {
        VeinMinerMaxBreakByLvl = new int[] {7,14,28};
        TreeFellerMaxBreakByLvl = new int[] {14,28,56};
        MaxDistanceFromPlayer = 8;
        MaxSoulBoundLevel = 10;
        ProtectItemFromBreaking = true;

        AutoSmeltWoodModifier = 0.15f;
        TameProtectsOnlyYourAnimals = true;
        chanceForEnderCurseToPreventDamage = 20;
        chanceForEnderCurseToTeleport = 40;
        perLevelChargeMultiplierForPotionRetention = 5;
        PotionRetentionMaxLevel = 10;
        veinMinerBlockWhiteList = new String[] {
            "minecraft:iron_ore",
            "minecraft:gold_ore",
            "minecraft:coal_ore",
            "minecraft:lapis_ore",
            "minecraft:diamond_ore",
            "minecraft:redstone_ore",
            "minecraft:emerald_ore",
            "minecraft:nether_quartz_ore",
            "minecraft:obsidian",
            "netherthings:nether_coal_ore",
            "netherthings:nether_iron_ore",
            "netherthings:nether_gold_ore",
            "netherthings:nether_redstone_ore",
            "netherthings:nether_lapis_ore",
            "netherthings:nether_emerald_ore",
            "netherthings:glowstone_ore",
            "netherthings:quartz_ore",
            "netherthings:nether_vibranium_ore",
            "refinedmachinery:copper_ore",
            "refinedmachinery:lead_ore",
            "refinedmachinery:silver_ore",
            "refinedmachinery:tin_ore",
            "refinedmachinery:nickel_ore",
            "techreborn:bauxite_ore",
            "techreborn:cinnabar_ore",
            "techreborn:copper_ore",
            "techreborn:galena_ore",
            "techreborn:iridium_ore",
            "techreborn:lead_ore",
            "techreborn:peridot_ore",
            "techreborn:pyrite_ore",
            "techreborn:ruby_ore",
            "techreborn:sapphire_ore",
            "techreborn:sheldonite_ore",
            "techreborn:silver_ore",
            "techreborn:sodalite_ore",
            "techreborn:sphalerite_ore",
            "techreborn:tin_ore",
            "techreborn:tungsten_ore",
        };
        EnableAutoSmelt = true;
        EnableEnderProtection = true;
        EnablePotionRetention = true;
        EnableTamedProtection = true;
        EnableTreeFeller = true;
        EnableVeinMiner = true;
        // EnableSentience = true;
        EnableSoulBound = true;
        EnableAccuracy = true;
        EnableInAccuracy = true;
        EnableArrowChaos = true;
        UseStandardSoulboundMechanics = false;
        AutoSmeltBlackList = new String[]
        {
            "minecraft:stone"
        };
        
    }

    public static MoEnchantsConfig init(MoEnchantsConfig config)
    {
        File file = new File(FabricLoader.getInstance().getConfigDirectory().getPath(), "moenchantconfig.json");

		try {
			// util.logger("loading config!", true);
			ModInit.logger.log("loading config!");
			FileReader fr = new FileReader(file);
			config = new Gson().fromJson(fr, MoEnchantsConfig.class);
			FileWriter fw = new FileWriter(file);
			fw.write(new GsonBuilder().setPrettyPrinting().create().toJson(config));
			fw.close();
		} catch (IOException e) {
			ModInit.logger.log("failed loading! Creating initial config!");
			// util.logger(, true);
			config = new MoEnchantsConfig();
			try {
				FileWriter fw = new FileWriter(file);
				fw.write(new GsonBuilder().setPrettyPrinting().create().toJson(config));
				fw.close();
			} catch (IOException e1) {
				ModInit.logger.log("failed config!");
				// util.logger("failed config!", true);
				e1.printStackTrace();
			}
        }
        return config;
    }




    






}