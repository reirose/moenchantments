package com.biom4st3r.moenchantments.interfaces;


/**
 * PlayerExperienceStore - Stores gained experience from mining with AlphaFire
 */
public interface PlayerExperienceStore
{
    public int getAndRemoveStoredExp();
    public void addExp(float i);
    


}