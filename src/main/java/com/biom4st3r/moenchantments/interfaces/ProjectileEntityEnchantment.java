package com.biom4st3r.moenchantments.interfaces;

/**
 * ProjectileEntityEnchantment
 */
public interface ProjectileEntityEnchantment {

    public void setAccuracyEnch(int i);
    public int getAccuacyEnch();
    public void setInAccuracyEnch(int i);
    public int getInAccuacyEnch();
    public void setChaosEnch(int i);
    public int getChaosEnch();

}