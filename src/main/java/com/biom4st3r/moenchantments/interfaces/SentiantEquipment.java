package com.biom4st3r.moenchantments.interfaces;

public interface SentiantEquipment
{
    
    public void talk();

    public int getFriendship();

    public void setFriendship(int val);

    public void incrementFriendship(int val);
    
    public boolean isSentiant();

}