package com.biom4st3r.moenchantments.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import net.minecraft.client.MinecraftClient;
import net.minecraft.network.ClientConnection;
import net.minecraft.util.hit.HitResult;

@Mixin(MinecraftClient.class)
public class MinecraftClientMixin 
{
    @Shadow
    public HitResult crosshairTarget;

    @Shadow
    private ClientConnection clientConnection;
    
    @Inject(at = @At("HEAD"), method = "doAttack")
    public void sendMissPacketForHerosEnchantment(CallbackInfo ci)
    {
        // if(crosshairTarget.getType() == Type.MISS)
        // {
        //     MinecraftClient.getInstance().getNetworkHandler().sendPacket(MoEnchantmentsModClient.createMissPacket());
        //     return;
        // }
    }
}