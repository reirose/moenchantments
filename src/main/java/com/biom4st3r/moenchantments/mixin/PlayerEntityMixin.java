package com.biom4st3r.moenchantments.mixin;

import com.biom4st3r.biow0rks.Mxn;
import com.biom4st3r.moenchantments.EnchantmentRegistry;
import com.biom4st3r.moenchantments.ModInit;
import com.biom4st3r.moenchantments.enchantments.MoEnchantment;
import com.biom4st3r.moenchantments.interfaces.PlayerExperienceStore;
import com.biom4st3r.moenchantments.interfaces.PotionEffectRetainer;
import com.biom4st3r.moenchantments.interfaces.PotionRetentionTarget;
import com.biom4st3r.moenchantments.logic.EnderProtectionLogic;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Unique;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.damage.DamageSource;
import net.minecraft.entity.effect.StatusEffectInstance;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.world.World;

@Mixin(PlayerEntity.class)
public abstract class PlayerEntityMixin extends LivingEntity implements PlayerExperienceStore {
    public PlayerEntityMixin(EntityType<? extends LivingEntity> entityType_1, World world_1) {
        super(entityType_1, world_1);
    }

    @Unique public float experienceStorage = 0;

    @Override
    public int getAndRemoveStoredExp() {
        if(experienceStorage >= 1f)
        {
            int experienceStorage_int = (int)experienceStorage;
            experienceStorage = experienceStorage-experienceStorage_int;
            ModInit.logger.debug("Claimed %s/%s exp", experienceStorage_int, experienceStorage);
            return experienceStorage_int;
        }
        return 0; 
    }

    @Inject(at = @At("HEAD"), method = "applyDamage",cancellable = true)
    public void enderProtectionImpl(DamageSource damageSource, float damage, CallbackInfo ci) {
        LivingEntity defender = (LivingEntity) (Object) this;
        int EnderProtectionLevel = EnchantmentHelper.getEquipmentLevel(EnchantmentRegistry.ENDERPROTECTION, defender);
        if (EnderProtectionLevel > 0) {
            if (EnderProtectionLogic.doLogic(damageSource, EnderProtectionLevel, defender)) 
            {
                // ci.setReturnValue(Boolean.FALSE);
                ci.cancel();
            }
        }
    }
    
    @Override
    public void addExp(float i) {
        ModInit.logger.debug("storing %s exp", i);
        this.experienceStorage += i;
    }

    @Inject(at = @At(Mxn.At.MethodHead),method = "attack")
    public void usePotionEffectFromImbued(Entity defender,CallbackInfo ci)
    {
        PotionEffectRetainer stack = ((PotionEffectRetainer) (Object) this.getMainHandStack());
        if(EnchantmentRegistry.POTIONRETENTION.isAcceptableItem(this.getMainHandStack()) && MoEnchantment.hasEnchant(EnchantmentRegistry.POTIONRETENTION, this.getMainHandStack()) && stack.getCharges() > 0)
        {
            StatusEffectInstance effect = stack.useEffect();
            if(defender instanceof LivingEntity)
            {
                
                ((PotionRetentionTarget)defender).applyRetainedPotionEffect(effect);
            }
        }
    }
}