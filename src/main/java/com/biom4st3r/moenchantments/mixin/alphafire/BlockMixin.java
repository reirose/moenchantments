package com.biom4st3r.moenchantments.mixin.alphafire;

import java.util.List;

import com.biom4st3r.moenchantments.logic.AlphafireLogic;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.item.ItemStack;
import net.minecraft.loot.context.LootContext;

@Mixin(Block.class)
public class BlockMixin
{
    @Inject(at = @At("HEAD"),method = "getDroppedStacks", cancellable = true)
    public void doAlphaFire(BlockState blockState, LootContext.Builder lootBuilder, CallbackInfoReturnable<List<ItemStack>> ci)
    {
        AlphafireLogic.doLogic( blockState,  lootBuilder, (Block)(Object)this, ci);
    }
}