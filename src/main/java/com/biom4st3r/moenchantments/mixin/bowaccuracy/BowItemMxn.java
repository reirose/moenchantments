package com.biom4st3r.moenchantments.mixin.bowaccuracy;

import com.biom4st3r.moenchantments.EnchantmentRegistry;
import com.biom4st3r.moenchantments.interfaces.ProjectileEntityEnchantment;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.LocalCapture;

import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.projectile.ProjectileEntity;
import net.minecraft.item.ArrowItem;
import net.minecraft.item.BowItem;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

@Mixin(BowItem.class)
public abstract class BowItemMxn
{

    @Inject(
        at = @At(
            value = "INVOKE",
            target = "net/minecraft/entity/projectile/ProjectileEntity.setProperties(Lnet/minecraft/entity/Entity;FFFFF)V"),
        method="onStoppedUsing",
        locals = LocalCapture.CAPTURE_FAILHARD)
    public void addEnchantmentsToArrowEntity(ItemStack stack, World world, LivingEntity user, int remainingUseTicks, CallbackInfo ci,PlayerEntity playerEntity, boolean bl, ItemStack itemStack, int i, float f, boolean bl2, ArrowItem arrowItem, ProjectileEntity projectileEntity)
    {
        ((ProjectileEntityEnchantment)projectileEntity).setAccuracyEnch(EnchantmentHelper.getLevel(EnchantmentRegistry.BOW_ACCURACY, stack));
        ((ProjectileEntityEnchantment)projectileEntity).setInAccuracyEnch(EnchantmentHelper.getLevel(EnchantmentRegistry.BOW_ACCURACY_CURSE, stack));
    }

}