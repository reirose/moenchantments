package com.biom4st3r.moenchantments.mixin.potion_retention;

import java.util.List;

import com.biom4st3r.moenchantments.enchantments.MoEnchantment;
import com.biom4st3r.moenchantments.interfaces.PotionEffectRetainer;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import net.minecraft.client.item.TooltipContext;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.text.LiteralText;
import net.minecraft.text.Text;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.Formatting;


@Mixin(ItemStack.class)
public abstract class ItemStackClientMixin
{
    @Inject(at = @At(value = "TAIL"),method = "getTooltip",cancellable = false)    
    public void getTooltipText(PlayerEntity playerEntity_1, TooltipContext tooltipContext_1,CallbackInfoReturnable<List<Text>> ci) 
    {
        List<Text> c = ci.getReturnValue();
        if(((PotionEffectRetainer) this).isPotionRetainer() > 0 && ((PotionEffectRetainer) this).getCharges() > 0)
        {
            for(int i = 0; i < c.size(); i++)
            {
                if(c.get(i) instanceof TranslatableText)
                {
                    if(((TranslatableText)c.get(i)).getKey().contains("potionretension"))
                    {
                        String name = new TranslatableText(((PotionEffectRetainer) this).getEffect().getTranslationKey(), new Object[0]).asFormattedString();
                        Text text = new LiteralText(
                            String.format(
                                "%s- %s %s%s %s/%s",
                                Formatting.GRAY,
                                name,
                                MoEnchantment.toRoman(((PotionEffectRetainer)this).getAmplification()+1),
                                Formatting.RESET,
                                ((PotionEffectRetainer) this).getCharges(),
                                ((PotionEffectRetainer) this).getMaxCharges()
                                ));
                        c.add(i+1,text);
                    }
                }
            }
        }
    }
}