package com.biom4st3r.moenchantments.mixin.potion_retention;

import java.util.Map;

import com.biom4st3r.moenchantments.ModInit;
import com.biom4st3r.moenchantments.interfaces.PotionRetentionTarget;
import com.google.common.collect.Maps;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.effect.StatusEffect;
import net.minecraft.entity.effect.StatusEffectInstance;
import net.minecraft.world.World;

@Mixin(LivingEntity.class)
public abstract class LivingEntityMixin extends Entity implements PotionRetentionTarget {
    protected LivingEntityMixin(EntityType<?> entityType_1, World world_1) {
        super(entityType_1, world_1); 
    }

    @Shadow
    protected abstract void onStatusEffectApplied(StatusEffectInstance statusEffectInstance_1);

    // @Shadow
    // protected abstract void method_6009(StatusEffectInstance statusEffectInstance_1, boolean boolean_1) ;

    @Shadow
    private final Map<StatusEffect, StatusEffectInstance> activeStatusEffects = Maps.newHashMap(); 

    @Shadow
    public abstract boolean canHaveStatusEffect(StatusEffectInstance sei);

    @Override
    public boolean applyRetainedPotionEffect(StatusEffectInstance newStatusEffect) 
    {
        if (!this.canHaveStatusEffect(newStatusEffect)) {
            return false;
         } else {
            StatusEffectInstance foundStatusEffect = (StatusEffectInstance)this.activeStatusEffects.get(newStatusEffect.getEffectType());
            if (foundStatusEffect == null) { // if not in there
               this.activeStatusEffects.put(newStatusEffect.getEffectType(), newStatusEffect);
               this.onStatusEffectApplied(newStatusEffect);
               ModInit.logger.debug( "adding new effect to activestatuseffects Map");
               return true;
            } else if (foundStatusEffect.getEffectType() == newStatusEffect.getEffectType() && foundStatusEffect.getAmplifier() == newStatusEffect.getAmplifier()) { //if has instance of
                foundStatusEffect = new StatusEffectInstance(foundStatusEffect.getEffectType(),foundStatusEffect.getDuration()+newStatusEffect.getDuration(),foundStatusEffect.getAmplifier());
                this.activeStatusEffects.replace(foundStatusEffect.getEffectType(), foundStatusEffect);
                this.onStatusEffectApplied(foundStatusEffect);
                ModInit.logger.debug("increased duration of effect in activestatuseffects map");
                return true;
            } else {
               return false;
            }
         }    
    }
}