package com.biom4st3r.moenchantments.mixin.potion_retention;

import java.util.Iterator;
import java.util.List;

import com.biom4st3r.moenchantments.ModInit;
import com.biom4st3r.moenchantments.interfaces.PotionEffectRetainer;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.LocalCapture;

import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.effect.StatusEffect;
import net.minecraft.entity.effect.StatusEffectInstance;
import net.minecraft.entity.thrown.ThrownPotionEntity;
import net.minecraft.util.math.Box;


@Mixin(ThrownPotionEntity.class)
public abstract class ThrownPotionEntityMixin
{
    @SuppressWarnings("rawtypes")
    @Inject(
        at=@At(
            value = "INVOKE_ASSIGN", 
            target="net/minecraft/entity/LivingEntity.addStatusEffect(Lnet/minecraft/entity/effect/StatusEffectInstance;)Z"
            ),
        method="applySplashPotion",
        locals = LocalCapture.CAPTURE_FAILHARD)
        private void addSplashPotionToWeapon(List<StatusEffectInstance> statusEffects, Entity targetEntity,CallbackInfo ci, Box bb, List l, Iterator i1,LivingEntity livingEntity_1, double double1,double double2,Iterator i2,StatusEffectInstance sei,StatusEffect se,int i)
        {
        StatusEffectInstance statuseffect = statusEffects.get(0);
        LivingEntity livingEntity;// = (LivingEntity) (Object) entity_1;
        PotionEffectRetainer mainhand;// = ((PotionEffectRetainer) (Object) livingEntity.getMainHandStack());
        PotionEffectRetainer offhand;// = ((PotionEffectRetainer) (Object) livingEntity.getOffHandStack());
        PotionEffectRetainer retainer;// = mainhand.isPotionRetainer() > 0 ? mainhand : offhand.isPotionRetainer() > 0 ? offhand : null;
        
        if(targetEntity != null && !targetEntity.world.isClient() && targetEntity instanceof LivingEntity)
        {
            livingEntity = (LivingEntity)targetEntity;
            mainhand = ((PotionEffectRetainer) (Object) livingEntity.getMainHandStack());
            offhand = ((PotionEffectRetainer) (Object) livingEntity.getOffHandStack());
            retainer = mainhand.isPotionRetainer() > 0 ? mainhand : offhand.isPotionRetainer() > 0 ? offhand : null;
            ModInit.logger.debug(statuseffect.getEffectType().getTranslationKey());
            if ((statuseffect != null && retainer != null) && ((retainer.getCharges() <= 0 ) || (statuseffect.getEffectType() == retainer.getEffect() && statuseffect.getAmplifier() == retainer.getAmplification()))) {
                int tokens = (int)Math.ceil((statuseffect.getDuration() / 20)/(10));
                retainer.setPotionEffectAndCharges(statuseffect, tokens);
            }
        }
        else if(livingEntity_1 != null)
        {
            //
            mainhand = ((PotionEffectRetainer) (Object) livingEntity_1.getMainHandStack());
            offhand = ((PotionEffectRetainer) (Object) livingEntity_1.getOffHandStack());
            retainer = mainhand.isPotionRetainer() > 0 ? mainhand : offhand.isPotionRetainer() > 0 ? offhand : null;
            if(retainer != null && ((retainer.getCharges() <= 0 ) || (statuseffect.getEffectType() == retainer.getEffect() && statuseffect.getAmplifier() == retainer.getAmplification())))
            {
                //int distanceModifyDuration = (int)(double2 * (double)statuseffect.getDuration() + 0.5D);
                int tokens = (int)Math.ceil((i / 20)/(10));
                retainer.setPotionEffectAndCharges(new StatusEffectInstance(se,0,statuseffect.getAmplifier()), tokens);
            }

            //((PotionRetentionTarget) livingEntity_1).applyRetainedPotionEffect(new StatusEffectInstance(statuseffect.getEffectType(),distanceModifyDuration,statuseffect.getAmplifier()));
        }
    }
}