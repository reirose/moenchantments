package com.biom4st3r.moenchantments.mixin.soulbound;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.biom4st3r.biow0rks.Mxn;
import com.biom4st3r.moenchantments.EnchantmentRegistry;
import com.biom4st3r.moenchantments.ModInit;
import com.biom4st3r.moenchantments.enchantments.MoEnchantment;

import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.Redirect;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.entity.ItemEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.util.DefaultedList;

@Mixin(PlayerInventory.class)
public abstract class PlayerInventoryMxn {
    @Shadow
    @Final
    private List<DefaultedList<ItemStack>> combinedInventory;
    @Shadow
    @Final
    public DefaultedList<ItemStack> main;
    @Shadow
    @Final
    public DefaultedList<ItemStack> armor;
    @Shadow
    @Final
    public DefaultedList<ItemStack> offHand;

    @Shadow
    @Final
    public PlayerEntity player;
    
    public List<ItemStack> mainBackup;
    public List<ItemStack> armorBackup;
    public List<ItemStack> offHandBackup;

    //@SuppressWarnings({ "unchecked", "rawtypes" })
    @Inject(at = @At(Mxn.At.MethodHead), method = "dropAll")
    public void backupSoulBoundItems(CallbackInfo ci) {
        mainBackup = new ArrayList<>(main);
        armorBackup = new ArrayList<>(armor);
        offHandBackup = new ArrayList<>(offHand);
    }

    @Redirect(at = @At(value = Mxn.At.BeforeInvoke, target = "Lnet/minecraft/entity/player/PlayerEntity;dropItem(Lnet/minecraft/item/ItemStack;ZZ)Lnet/minecraft/entity/ItemEntity;"), method = "dropAll")
    public ItemEntity maybeDropItem(PlayerEntity pe, ItemStack itemStack_1, boolean boolean_1, boolean boolean_2) {
        if (MoEnchantment.hasEnchant(EnchantmentRegistry.SOULBOUND, itemStack_1)) {
            return null;
        } else {
            return pe.dropItem(itemStack_1, boolean_1, boolean_2);
        }
    }

    private void downgradeSoulBound(ItemStack iS)
    {
        if(!ModInit.config.UseStandardSoulboundMechanics)
        {
            Map<Enchantment, Integer> enchantments = EnchantmentHelper.getEnchantments(iS);
            int soulboundLvl = EnchantmentHelper.getLevel(EnchantmentRegistry.SOULBOUND, iS);
            iS.removeSubTag("Enchantments");
            enchantments.remove(EnchantmentRegistry.SOULBOUND);
            for(Enchantment ench : enchantments.keySet())
            {
                iS.addEnchantment(ench, enchantments.get(ench));
            }
            if(soulboundLvl > 1)
            {
                iS.addEnchantment(EnchantmentRegistry.SOULBOUND, soulboundLvl-1);
            }
        }
    }

    @Inject(at = @At(Mxn.At.BeforeReturn), method = "dropAll")
    public void restoreSoulBoundItems(CallbackInfo ci) {
        ItemStack current;
        for (int i = 0; i < mainBackup.size(); i++) {
            if (MoEnchantment.hasEnchant(EnchantmentRegistry.SOULBOUND, mainBackup.get(i))) {
                //System.out.println("found soulbound in main");
                current = mainBackup.get(i).copy();
                main.set(i, current);
                downgradeSoulBound(current);

            }
        }
        for (int i = 0; i < armorBackup.size(); i++) {
            if (MoEnchantment.hasEnchant(EnchantmentRegistry.SOULBOUND, armorBackup.get(i))) {
                //System.out.println("found soulbound in armor");
                current = armorBackup.get(i).copy();
                armor.set(i, current);
                downgradeSoulBound(current);

            }
        }
        for(int i = 0; i < offHandBackup.size(); i++)
        {

            if(MoEnchantment.hasEnchant(EnchantmentRegistry.SOULBOUND, offHandBackup.get(i)))
            {
                //System.out.println("found soulbound in offHand");
                current = offHandBackup.get(i).copy();
                offHand.set(i, current);
                downgradeSoulBound(current);

            }
        }
        mainBackup = null;
        armorBackup = null;
        offHandBackup = null;
    }

}