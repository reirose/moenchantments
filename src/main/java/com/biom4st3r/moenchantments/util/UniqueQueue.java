package com.biom4st3r.moenchantments.util;

import java.util.PriorityQueue;

/**
 * UniqueQueue - prevents duplicates
 */
public class UniqueQueue<T> extends PriorityQueue<T> {

    private static final long serialVersionUID = 2163057383757900549L;

    @Override
    public boolean offer(T e) {

        if(this.contains(e))
        {
            return false;
        }
        return super.offer(e); 
    }
}